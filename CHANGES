---- Version 0.8 -> 0.8.1
o Added reasonable intelligence based on
  - Finishing the game
  - Check
  - Capture
  - Castling and Promotion
  - Proximity to end of board.
  These are very basic and frequently wrong guesses, but they are
  better than previous versions.  All possible moves are checked.
  There is almost no lookahead but there is a little asynchronicity
  (stuff can happen while the computer is working).  It's very easy
  to win, but it does teach moves.  Watch out for the computer's
  cannons; it knows how to use them...

o Fixed problem concerning PieceThreatened and promoted pieces,
  simplified the routine hugely, and renamed it to SquareThreatened.

o Optimised TraverseDir by stopping checking in a direction if a piece
  is hit or the edge of the board is encountered.  Macroized many
  of the complexities of directions.

o Discovered that Solaris has function prototypes, so took that into
  consideration in mahine.h

o Moved back to X11R5 compatibility.  Using XlibSpecificationRelease
  to determine which version is being compiled.

o Fixed problem which allowed promotion if the pawn's king was in check.

o Fixed bug re: bishop, which allowed it to move diagonally in *any* two
  directions, even though it can actually move diagonally in only two
  way: strictly on the one board (a la chess) or like this and between
  boards.  That is, a move from Yc1 to Zc2 is illegal.  Additionally
  fixed bug which prevented it from changing levels.

o Fixed bug with knights which meant that the computer thought that it
  move them from row 7 to row 0 via an ABS() oversight.

o Arranged board windows so that they tend not to overlap at startup.

o Minor editing of the docs.

---- Version 0.7.6 -> 0.8
o No changes.  I just decided that enough was done in 0.7.6 (which was
  never released) to justify the version bump.

---- Version 0.7.5 -> 0.7.6

o Some minor new functionality; slightly more descriptive messages;
  beeps are less frequent.

o Reduced the size of xif.c by putting some code into callbaks.c and
  some into xnet.c.  Moved the X stuff in 3Dc.h into x3Dc.h

o Networking code is eliminated.  It was causing portability problems.
  "What!" I hear you exclaim---"No more network 3Dc?!"  Don't worry,
  I've merely realised what a plonker I really am and moved over to
  X; after all, it is a networking GUI so why not use it?  See the new
  "-ad" command-line option.

o All the AI code is now in ai.c; it has changed quite a bit.
  Several bugs in the AI code found and fixed.  The
  associated global variables (DontMove) are now module
  variables, local to their respective files.  Only Board and bwToMove
  are global now.

o Move some of the code from pieceThreatened and similar functions into
  the new function TraverseDir, added a DIR macro and added direction defines.
  This was primarily to reduce function sizes and reduce nesting.

o Added a make install (how did I miss putting this in ages ago?)
  Got rid of that dodgy make config code in favour of a user-modifyable
  Makefile.  Made an undo option configurable from the Makefile.

o Fixed a few bugs in engine.c and piece.c which allowed a few dodgy
  things when moving a king out of check and another bug in engine.c,
  which caused a seg fault if you moved into one of eight spaces on the
  top board.  At least one precompiler bug was found and fixed.

o Changed function names to all start with capital letters.  Variables
  still start with small letter.

---- Version 0.7.4 -> 0.7.5

Only few (tiny) changes this time

 o namelen in net.c is now set to sizeof(SOCKADDR) before calling accept.
   Thanks Robert.

 o string.h is now included by xif.c and main.c, and references to strdup
   and sprintf put into machine.h

---- Version 0.7.3 -> 0.7.4

o Investigated the use of pixmap cursors again, and came up with the following
  on the way:

  - Put in pixmap cursors (you can go back to using the cursor font by defining
    FONTCURSOR in xif.c).

  - Memory leak; only need to call XCreateFontCursor once (I don't call
    XFreeCursor, as it should be autofreed on quitting.  Bad practice, I
    know, but it means that I don't have to make cursor a global.  This
    bug doesn't apply to pixmap cursors, as I create the cursor each time,
    rather than maintain 22 cursors).

  - Fixed bug whereby the cursor would only be changed for the current board
    and any above it; boards below wouldn't have it changed (this one applies
    to both the new pixmap cursors and the older font cursors).

  - Fixed bug where the cursor would change even if nothing was picked up.
    This wasn't serious for FONTCURSOR, but segfaulted with pixmap cursors.

o Halved the number of masks required (thus saving the space required for
  11 32x32x1 pixmaps and the relevant pointers and IDs - talk about
  conscientious..)

o Fixed redrawing of muster window during restart (i.e. now it gets redrawn).

o Fixed a bug with undo - the undone piece thought that it was in both its
  original place and the place from which it was undone.

o Made the "Resign Game?" dialog pop up in a more sensible position.  It
  doesn't check to see if some of the dialog has gone missing off the edge of
  the screen, but I don't care - do you?

o Improved the net play a bit.  Now restart and resign work over the net.
  There's no verification by the other player, but the whole net stuff is
  hanging together by a thread anyway.  I'll totally rewrite it one year.

---- Version 0.7.2 -> 0.7.3

o Fixed the problem whereby the game would crash when trying to put up the
  promotion callback.  So now you can have 25 Queens..

o Taking En Passant would remove the piece from the board, but not from the
  game (this could be seen by examining the muster window while taking en
  passant - nothing would change).  This is now fixed.

o Fixed bug whereby a pawn taking a piece on the final rank would destroy
  the piece utterly, even unto undos.

o Removed some dependance on the shape of the boards; this will simplify
  extending the game to arbitrarily sized boards, with arbitrarily many
  players.

o Discovered that my fix to the makefile only works for GNU make; the real
  fix is to create an empty file .config, and to change the -f in the test
  to a -s; now it works.

---- Version 0.7.1 -> 0.7.2
Small changes, again.  The muster window got most of the attention.

o The following changes were made in the muster window:

  - The black royalty had the count of the black nobility, and vice versa.
  - The black royalty and nobility were too far to the left.
  - The Pawns' count vertical position was dependant on the width of the muster
    window - a silly mistake.

o Fixed memory leak in Err3Dc.

o Fixed bug with pawn allowing it to move 2 forward, 1 sideways (like a knight)
  on its first move.

o Got the make working properly, so that you don't have to make config first;
  now it'll do it for you automatically.

o Really got that truncated status line bug fixed.  It now auto-centres (sic)
  the status line, too.

---- Version 0.7 -> 0.7.1
Not too much, this time.

o Muster window added, showing how many pieces of each type and colour are
  still in play.

o Improved exposure-handling; now only exposed squares get drawn.  It's too
  complicated to draw just exposed pixels.  Also, it's not worth doing the
  same in the muster window; there's not much in there.

o Bug fixes, including truncated status line, and bad redrawing of enlarged
  boards (that one had me stumped for a while..)

---- Version 0.6 -> 0.7
Big changes this time, deserving the creation of this file.

o Changed from Xm to Xaw (with DrawingArea widget; see ACKNOWLEDGEMENTS),
  which involved a lot of necessary (and usually highly cool) changes:

  - Changed from one DrawnButton per square to one DrawingArea per level.
  - Can trap ButtonPress and ButtonRelease much easier, so implemented
    drag-and-drop moving instead of src-dest-ok clicking.
  - Removed the Source and Dest fields in the main window.
  - Reduced size of internal representations of the muster and graphics.

o Now change cursor while move is in progress (I'm working on changing
  the cursor to a pixmap of the piece being moved, but getting Xpm to
  generate monochrome pixmaps is harder than I thought..)

o Removed last message from message-box if it is no longer applicable.

o Got rid of that irritating beep all the time - now it only happens
  on the events which are both "interesting" and need your attention
  (such as your opponent moving).

o Added some measure of machine configuration and autodetection.

o Fixed an undo bug, a bug with kings and checking them, and one or two
  others.


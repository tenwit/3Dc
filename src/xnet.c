/*
 * The TCP interface for 3Dc
 */
/*

    3Dc, a game of 3-Dimensional Chess
    Copyright (C) 1995  Paul Hicks

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    E-Mail: paulh@euristix.ie
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>

#include "machine.h"

/* AutoInclude all necessary files */
#include "3Dc.h"
#include <X11/Shell.h>

Local GfxInfo GFX2;

Global Boolean
Open2ndDisplay(const char *displayName)
{
  XtAppContext app;
  XColor col, dummy;
  int x;
  Display *display;

  secondGFX = &GFX2;

  app = XtCreateApplicationContext();
  x = 0;
  display = XtOpenDisplay(app, displayName, "3Dc", "3Dc", NULL, 0, &x, NULL);
  secondGFX->mainWindow =
    XtAppCreateShell("3Dc", "3Dc", applicationShellWidgetClass, display,
                     NULL, 0);

#ifdef FONTCURSOR
  secondGFX->monoGC = NULL;
#endif /* FONTCURSOR */
  secondGFX->gc = XDefaultGCOfScreen(XtScreen(secondGFX->mainWindow));
  XSetFunction(display, secondGFX->gc, GXcopy);

  secondGFX->whitePixel = WhitePixelOfScreen(XtScreen(secondGFX->mainWindow));
  secondGFX->blackPixel = BlackPixelOfScreen(XtScreen(secondGFX->mainWindow));

  /* Make two attempts at getting a grey colour. */
  if (!XAllocNamedColor(display,
                    DefaultColormapOfScreen(XtScreen(secondGFX->mainWindow)),
                        "grey", &col, &dummy))
    {
      if (!XAllocNamedColor(display,
                 DefaultColormapOfScreen(XtScreen(secondGFX->mainWindow)),
                            "light grey", &col, &dummy))
        secondGFX->greyPixel = secondGFX->blackPixel;
      else
        secondGFX->greyPixel = col.pixel;
    }
  else
    secondGFX->greyPixel = col.pixel;

  if (InitPixmaps( secondGFX ))
    return 1;
  if (InitMainWindow( secondGFX ))
    return 1;
  if (InitBoardWindows( secondGFX ))
    return 1;

  XtRealizeWidget(secondGFX->mainWindow);

  return 0;
}

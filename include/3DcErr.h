/*
 * This file defines everything to do with error-handling
 * that is unique to 3Dc.
 */
/*

    3Dc, a game of 3-Dimensional Chess
    Copyright (C) 1995  Paul Hicks

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    E-Mail: paulh@euristix.ie
*/
#ifndef __3DcErr_h
#define __3DcErr_h
extern int n3DcErr;

typedef enum
{
  E3DcSIMPLE, E3DcLEVEL, E3DcCHECK, E3DcDIST, E3DcINVIS,
  E3DcBLOCK, E3DcMOVED
} Error;

typedef struct
{
  Error nErrNum;
  char *pszErrStr;
} error_t;

/*
 * All strings are designed to be printed thus:
 *      printf("That piece %s.\n");
 */
/* To be defined by interface */
extern int Err3Dc(const GfxInfo *, const char *, const Boolean);

#endif /* __3DcErr_h */


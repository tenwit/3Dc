/*
 * This file is supposed to get over any machine-dependent
 * problems.  Some, like ulimit(2) are fixed in the kernel (usually);
 * the rest are up to us users to work out
*/
/*

    3Dc, a game of 3-Dimensional Chess
    Copyright (C) 1995  Paul Hicks

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    E-Mail: paulh@euristix.ie
*/
#ifndef _3DC_MACHINE_H
#define _3DC_MACHINE_H

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_ULIMIT_H
#include <ulimit.h>
#endif

#ifdef __alpha__
#include <alloca.h>
#endif /* __alpha__ */

#ifdef SYSV
#define random() rand()
#define srandom(a) srand(a)
#endif /* SYSV */

#ifdef sun
#define XtSetLanguageProc(a,b,c) 
#endif /* sun */

#ifdef __GNUC__
#define INLINE __inline__
#else
#define INLINE
#endif /* __GNUC__ */

#endif /* _3DC_MACHINE_H */

/*
 * local.h
 *
 * Application-independant defines.
 */
#ifndef _LOCAL_H
#define _LOCAL_H

#ifndef Boolean
#define Boolean int
#endif /* Boolean */
#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif /* True */
#ifndef NULL
#define NULL 0
#endif /* NULL */
#ifndef ABS
#define ABS(a) ((a) < 0 ? -(a) : (a))
#endif /* ABS */
#ifndef MAX
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) > (b) ? (b) : (a))
#endif /* MAX */

/* Abstractions */
#define Global extern
#define Local  static

#ifdef DEBUG
# define D(debug_code) debug_code
# define CHECK(bool) ((bool)?1:(fprintf(stderr, "Assertion failed on line %i of %s\n", __LINE__, __FILE__),0))
#else /* DEBUG */
# define D(debug_code)
# define CHECK(bool) ((bool)?1:0)
#endif /* DEBUG */

/* The empty function (for empty loops etc.) */
#define nop()
/* The fall-through case for switch statements */
#define FallThrough()

#ifdef GCC
#define Inline inline
#else /* GCC */
#define Inline
#endif /* GCC */

#endif /* _LOCAL_H */

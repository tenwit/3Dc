/*
 * x3Dc.h
 *
 * Definitions file for 3Dc X interface
 */
/*

    3Dc, a game of 3-Dimensional Chess
    Copyright (C) 1995  Paul Hicks

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    E-Mail: paulh@euristix.ie
*/
#ifndef __x3Dc_H
#define __x3Dc_H

#include <X11/Intrinsic.h> /* For all other X stuff */
#include "local.h"

/*****************************************************************************
 * Platform-dependent graphics info
 */
typedef struct
{
  GC
#ifndef FONTCURSOR
    monoGC,             /* Mono GC for cursor */
#endif /* FONTCURSOR */
    gc;                 /* Graphics context */
  Widget
    mainWindow,                                /* Toplevel widget */
    muster,                                    /* Display area    */
    remark,                                    /* Message area    */
    undo,                                      /* Undo button     */
    board[LEVELS];                             /* Playing areas   */
  Dimension width[LEVELS], height[LEVELS];     /* Sizes of playing areas */
  Pixel blackPixel, whitePixel, greyPixel;     /* Colours */
  Pixmap face[COLOURS][TITLES],  mask[TITLES]; /* Pixmaps */
  XFontStruct *font;                           /* The display font */
} GfxInfo;

Global GfxInfo *firstGFX, *secondGFX;
#define XPM_SIZE 32

/*
 * End of graphics info
 ****************************************************************************/

/****************************************************************************/
/* Functions */
/* Interface stuff */
Global int Init3DcGFX(int, char **);
Global int InitPixmaps( GfxInfo * );
Global int InitMainWindow( GfxInfo * );
Global int InitBoardWindows( GfxInfo * );
Global void Draw3DcBoard(void);
Global void UpdateMuster(Colour, Title, Boolean);
Global void PieceDisplay(const Piece *, const Boolean);
Global void PiecePromote( Piece * );

/* 2nd interface stuff */
Global Boolean Open2ndDisplay(const char *);

/* Callbacks */
Global void DrawBoard(Widget, XtPointer, XtPointer);
Global void DrawMuster(Widget, XtPointer, XtPointer);
Global void ResignGame(Widget, XtPointer, XtPointer);
Global void MouseInput(Widget, XtPointer, XtPointer);
Global void PromotePiece(Widget, XtPointer, XtPointer);
Global void CancelDialog(Widget, XtPointer, XtPointer);
Global void UndoMove(Widget, XtPointer, XtPointer);
Global void Restart3Dc(Widget, XtPointer, XtPointer);
Global void Quit3Dc(Widget, XtPointer, XtPointer);

/* Useful macros */
#define SQ_POS_X(gfx, boardNum, x) \
   (((gfx->width[(boardNum)]%FILES) / 2) + \
    ((gfx->width[(boardNum)]/FILES) * (x)))
#define SQ_POS_Y(gfx, boardNum, y) \
  (((gfx->height[(boardNum)]%RANKS) / 2) + \
   ((gfx->height[(boardNum)]/RANKS) * (y)))

#endif /* __x3Dc_h */
